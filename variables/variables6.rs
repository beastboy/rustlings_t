// variables6.rs
// Make me compile! Execute the command `rustlings hint variables6` if you want a hint :)

const NUMBER : u32 = 1;
fn main() {
    println!("Number {}", NUMBER);
}
